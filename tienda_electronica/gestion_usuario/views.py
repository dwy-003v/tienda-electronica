from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from .forms import RegistrationForm, LoginForm
from django.contrib.auth import logout, authenticate, login
# Create your views here.

def logout_view(request):
    #thread
    logout(request)
    return HttpResponseRedirect(reverse('gestion_usuario:login'))

def login_view(request):
    if request.method == 'POST':
        login_form = LoginForm(data=request.POST)
        if login_form.is_valid():
            username= login_form.clean().get("username")
            password = login_form.clean().get("password")
            user = authenticate(request, username=username, password=password)
            #request.user None (vacio, nulo, sin espacio asignado) 
            if user is not None:
                login(request, user)

            #request.user Objeto (ocupado, ocupa espacio) 
            if request.user.is_authenticated:
                return HttpResponseRedirect(reverse('tienda:index'))
            else:
                return render(request, 'gestion_usuario/login.html',
                              {
                                  'login_form': login_form,
                                  'message': 'Usuario invalido'
                              })

    login_form = LoginForm()
    return render(request, 'gestion_usuario/login.html',
                  {
                      'login_form': login_form,
                  })


def registrar(request):
    if request.method == 'POST':
        user_form = RegistrationForm(data=request.POST)
        if user_form.is_valid():
            user_form.save()
            return HttpResponseRedirect(reverse('gestion_usuario:login'))
        else:
            return render(request, 'gestion_usuario/registrar.html',
                          {
                              'user_form': user_form
                          })

    user_form = RegistrationForm()
    return render(request, 'gestion_usuario/registrar.html',
                  {
                      'user_form': user_form
                  })
