from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm


class RegistrationForm(UserCreationForm):

    def __init__(self, *args, **kwargs):

        super(RegistrationForm, self).__init__(*args,**kwargs)

        for field in self.fields:            
            self.fields[field].widget.attrs.update({'class': 'form-control'})

 
        for field in self.fields.values():
            field.help_text = None

class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        for field in self.fields:            
            self.fields[field].widget.attrs.update({'class': 'form-control'})

        for field in self.fields.values():
            field.help_text = None