from django.http.response import HttpResponse, HttpResponseNotAllowed
from django.shortcuts import redirect, render
from django.urls.base import reverse
from .models import Tienda
from django.contrib.auth.models import User
from .forms import TiendaForm
from django.contrib.auth.decorators import login_required, permission_required
# Create your views here.


def index(request):
    return render(request, 'administracion/index.html', {})

@login_required(login_url='gestion_usuario:login')
@permission_required('administracion.add_tienda', login_url='gestion_usuario:login')
def crear(request):
    #if request.user.is_authenticated:
    #   if request.user.has_perm('admnisitracion.tienda.add_tienda'):
    #        pass
    #   else:
    #       return HttpResponse("No tiene permiso")
    #       return HttpResponseRedirect(reverse('gestion_usuario:login'))
    #else:
    #   return HttpResponseRedirect(reverse('gestion_usuario:login'))
    
    if request.method == 'GET':
        tienda_form = TiendaForm()
        #request.GET.get('')
        message = 'recibi un metodo get'
        context = {
            'message': message,
            'tienda_form': tienda_form
        }
        return render(request, 'administracion/crear2.html', context)

    elif request.method == 'POST':
        message = ''
        tienda_form = TiendaForm(data=request.POST)
        if tienda_form.is_valid():
            message = 'Válido'
            tienda_form.save()
        else:
            message = tienda_form.errors


        contexto =  {
            #'message' : message,
            'tienda_form': tienda_form,
        }
        return render(request, 'administracion/crear2.html', contexto)
    else:
        return HttpResponseNotAllowed()
        
