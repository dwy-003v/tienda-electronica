from django import forms
from django.forms import widgets
from django.forms.models import fields_for_model
from .models import Tienda
from django.contrib.auth.models import User


class TiendaForm(forms.ModelForm):
    class Meta():
        model = Tienda
        fields = ('nombre_tienda', 'direccion', 'ciudad', 'comuna',
                  'teléfono', 'correo_electronico', 'encargado')
       

    def __init__(self, *args, **kwargs):
        super(TiendaForm, self).__init__(*args,**kwargs)
        self.fields['encargado'] = forms.ModelChoiceField(queryset=User.objects.all())
        
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        



